# 设计模式学习示例

## 软件设计原则
- open_close_principle 开闭原则
- liskov_substitution_principle 里氏代换原则
- dependence_inversion_principle 依赖倒转原则
- interface_isolation_principle 接口隔离原则
- law_of_demeter 迪米特法则
- composite_reuse_principle 合成复用原则

设计模式

- singleton 单例
- 
