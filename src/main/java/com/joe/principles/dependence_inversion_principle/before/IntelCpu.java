package com.joe.principles.dependence_inversion_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class IntelCpu {

    public void run(){
        System.out.println("运行IntelCpu");
    }
}
