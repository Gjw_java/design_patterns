package com.joe.principles.dependence_inversion_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class XiJieHardDisk {

    public void save(String data){
        System.out.println("使用希捷硬盘保存数据：" + data);
    }

    public String get(){
        System.out.println("使用希捷硬盘读取数据");
        return "数据";
    }
}
