package com.joe.principles.dependence_inversion_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class KingstonMemory {

    public void save(){
        System.out.println("使用金士顿内存条");
    }
}
