package com.joe.principles.dependence_inversion_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class ComputerDemo {

    public static void main(String[] args) {
        Computer computer = new Computer();
        XiJieHardDisk hardDisk = new XiJieHardDisk();
        IntelCpu cpu = new IntelCpu();
        KingstonMemory memory = new KingstonMemory();
        computer.setHardDisk(hardDisk);
        computer.setCpu(cpu);
        computer.setMemory(memory);
        computer.run();
    }
}
