package com.joe.principles.dependence_inversion_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class Computer {

    private HandDisk hardDisk;
    private Cpu cpu;
    private Memory memory;

    public HandDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(HandDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public Cpu getCpu() {
        return cpu;
    }

    public void setCpu(Cpu cpu) {
        this.cpu = cpu;
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    public void run(){
        System.out.println("运行计算机");
        String data = hardDisk.get();
        System.out.println("从硬盘上获取的数据: " + data);
        cpu.run();
        memory.save();
    }
}
