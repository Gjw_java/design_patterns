package com.joe.principles.dependence_inversion_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public interface Memory {

    void save();
}
