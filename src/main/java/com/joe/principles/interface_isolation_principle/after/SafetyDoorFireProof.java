package com.joe.principles.interface_isolation_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public interface SafetyDoorFireProof {

    void fireProof();
}
