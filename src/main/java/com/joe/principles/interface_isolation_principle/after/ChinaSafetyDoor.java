package com.joe.principles.interface_isolation_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class ChinaSafetyDoor implements SafetyDoorAntiTheft, SafetyDoorFireProof{

    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("防火");
    }
}
