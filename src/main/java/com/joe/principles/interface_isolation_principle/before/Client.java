package com.joe.principles.interface_isolation_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Client {

    public static void main(String[] args) {
        CustomerSafetyDoor door = new CustomerSafetyDoor();
        door.antiTheft();
        door.fireProof();
        door.waterProof();
    }
}
