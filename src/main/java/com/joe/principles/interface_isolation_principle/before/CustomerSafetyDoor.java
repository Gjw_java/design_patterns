package com.joe.principles.interface_isolation_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class CustomerSafetyDoor implements SafetyDoor{

    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("防火");
    }

    @Override
    public void waterProof() {
        System.out.println("防水");
    }
}
