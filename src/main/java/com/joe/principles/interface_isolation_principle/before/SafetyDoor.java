package com.joe.principles.interface_isolation_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public interface SafetyDoor {

    void antiTheft();

    void fireProof();

    void waterProof();
}
