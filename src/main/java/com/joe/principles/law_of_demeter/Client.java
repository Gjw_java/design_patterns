package com.joe.principles.law_of_demeter;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Client {

    public static void main(String[] args) {
        Agent agent = new Agent();
        Star star = new Star("黄晓明");
        agent.setStar(star);
        Fans fans = new Fans("joe");
        agent.setFans(fans);
        Company company = new Company("腾讯");
        agent.setCompany(company);

        agent.meeting();

        agent.business();
    }
}
