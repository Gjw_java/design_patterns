package com.joe.principles.law_of_demeter;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Fans {

    private String name;

    public Fans(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
