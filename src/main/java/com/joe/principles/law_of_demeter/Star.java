package com.joe.principles.law_of_demeter;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Star {

    private String name;

    public Star(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
