package com.joe.principles.liskov_substitution_principle.after;

/**
 * @Description 长方形
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class Rectangle implements Quadrilateral {

    private double length;
    private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
