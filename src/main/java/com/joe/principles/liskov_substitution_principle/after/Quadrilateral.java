package com.joe.principles.liskov_substitution_principle.after;

/**
 * @Description 四边形
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public interface Quadrilateral {

    public double getLength();

    public double getWidth();
}
