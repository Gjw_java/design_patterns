package com.joe.principles.liskov_substitution_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class RectangleDemo {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setLength(10);
        rectangle.setWidth(5);
        resize(rectangle);
        printLengthAndWidth(rectangle);

        Square square = new Square();
        square.setSide(10);
        printLengthAndWidth(square);
    }

    // 扩宽方法
    public static void resize(Rectangle rectangle){
        while (rectangle.getWidth() <= rectangle.getLength()){
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    //打印长和宽
    public static void printLengthAndWidth(Quadrilateral quadrilateral){
        System.out.println(quadrilateral.getLength());
        System.out.println(quadrilateral.getWidth());
    }
}
