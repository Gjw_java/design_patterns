package com.joe.principles.liskov_substitution_principle.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class Square implements Quadrilateral {

    private double side;


    public void setSide(double side){
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    @Override
    public double getLength() {
        return side;
    }

    @Override
    public double getWidth() {
        return side;
    }
}
