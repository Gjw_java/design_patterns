package com.joe.principles.liskov_substitution_principle.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class Square extends Rectangle {

    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width) {
        super.setLength(width);
        super.setWidth(width);
    }
}
