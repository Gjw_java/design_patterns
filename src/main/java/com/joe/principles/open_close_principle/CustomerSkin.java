package com.joe.principles.open_close_principle;

/**
 * @Description 自定义皮肤
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class CustomerSkin extends AbstractSkin{

    @Override
    public void display() {
        System.out.println("自定义皮肤");
    }
}
