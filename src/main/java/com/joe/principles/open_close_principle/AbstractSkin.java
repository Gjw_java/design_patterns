package com.joe.principles.open_close_principle;

/**
 * @Description 抽象皮肤类
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public abstract class AbstractSkin {

    // 显示的方法
    public abstract void display();

}
