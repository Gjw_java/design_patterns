package com.joe.principles.open_close_principle;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class Client {

    public static void main(String[] args) {
        SougouInput sougouInput = new SougouInput();
        DefaultSkin defaultSkin = new DefaultSkin();
        CustomerSkin customerSkin = new CustomerSkin();
        sougouInput.setSkin(customerSkin);
        sougouInput.display();
    }
}
