package com.joe.principles.open_close_principle;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class SougouInput {

    private AbstractSkin skin;

    public void setSkin(AbstractSkin skin){
        this.skin = skin;
    }

    public void display(){
        skin.display();
    }
}
