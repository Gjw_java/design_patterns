package com.joe.principles.open_close_principle;

/**
 * @Description 默认皮肤
 * @Author 高建伟-joe
 * @Date 2023-12-25
 */
public class DefaultSkin extends AbstractSkin {

    @Override
    public void display() {
        System.out.println("默认皮肤");
    }
}
