package com.joe.pattern.combination;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class Client {

    public static void main(String[] args) {
        MenuComponent menu = createMenu();
        menu.print();
        new MenuItem("页面访问", 3).add(new MenuItem("1",1));
    }

    private static MenuComponent createMenu(){
        MenuComponent menu1 = new Menu("菜单管理", 2);
        menu1.add(new MenuItem("页面访问", 3));
        MenuComponent menu2 = new Menu("权限配置", 2);
        menu2.add(new MenuItem("提交保存", 3));
        MenuComponent menu3 = new Menu("角色管理", 2);
        menu3.add(new MenuItem("新增角色", 3));

        MenuComponent menu = new Menu("系统管理", 1);
        menu.add(menu1);
        menu.add(menu2);
        menu.add(menu3);
        return menu;
    }
}
