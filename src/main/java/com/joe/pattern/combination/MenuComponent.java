package com.joe.pattern.combination;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public abstract class MenuComponent {

    protected String name;
    protected int level;

    public void add(MenuComponent menuComponent){
        throw new UnsupportedOperationException();
    }

    public void remove(MenuComponent menuComponent){
        throw new UnsupportedOperationException();
    }

    public MenuComponent getChild(int index){
        throw new UnsupportedOperationException();
    }

    public String getName(){
        return name;
    }

    public abstract void print();
}
