package com.joe.pattern.template;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class Client {
    public static void main(String[] args) {
        AbstractClass baoCai = new ConcreteClass_BaoCai();
        baoCai.cookProcess();
        AbstractClass caiXin = new ConcreteClass_CaiXin();
        caiXin.cookProcess();
    }
}
