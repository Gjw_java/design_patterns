package com.joe.pattern.template;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public abstract class AbstractClass {

    public final void cookProcess(){
        pourOil();
        heatOil();
        pourVegetable();
        pourSauce();
        fry();
    }

    public void pourOil(){
        System.out.println("倒油");
    }

    public void heatOil(){
        System.out.println("煎油");
    }

    public abstract void pourVegetable();

    public abstract void pourSauce();

    public void fry(){
        System.out.println("翻炒");
    }
}
