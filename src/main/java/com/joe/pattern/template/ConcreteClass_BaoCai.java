package com.joe.pattern.template;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class ConcreteClass_BaoCai extends AbstractClass{
    @Override
    public void pourVegetable() {
        System.out.println("包菜");
    }

    @Override
    public void pourSauce() {
        System.out.println("辣椒");
    }
}
