package com.joe.pattern.template;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class ConcreteClass_CaiXin extends AbstractClass{
    @Override
    public void pourVegetable() {
        System.out.println("菜心");
    }

    @Override
    public void pourSauce() {
        System.out.println("蒜蓉");
    }
}
