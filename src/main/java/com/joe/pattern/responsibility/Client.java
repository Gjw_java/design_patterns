package com.joe.pattern.responsibility;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Client {
    public static void main(String[] args) {
        LeaveRequest leave = new LeaveRequest("小明", 5, "身体不适");
        GroupLeader groupLeader = new GroupLeader();
        Manager manager = new Manager();
        GeneralManager generalManager = new GeneralManager();
        groupLeader.setNextHandler(manager);
        manager.setNextHandler(generalManager);
        groupLeader.submit(leave);
    }
}
