package com.joe.pattern.responsibility;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Manager extends Handler{

    public Manager(){
        super(Handler.NUM_ONE, Handler.NUM_THREE);
    }

    @Override
    protected void handlerLeave(LeaveRequest leave) {
        System.out.println("部门经理审批：");
        System.out.println(leave.getName() + "请假" + leave.getNum() + "天，原因"+ leave.getContent());
    }
}
