package com.joe.pattern.responsibility;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class GeneralManager extends Handler{

    public GeneralManager(){
        super(Handler.NUM_THREE, Handler.NUM_SEVEN);
    }

    @Override
    protected void handlerLeave(LeaveRequest leave) {
        System.out.println("总经理审批：");
        System.out.println(leave.getName() + "请假" + leave.getNum() + "天，原因"+ leave.getContent());
    }
}
