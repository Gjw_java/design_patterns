package com.joe.pattern.facade;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class Client {

    public static void main(String[] args) {
        SmartAppliancesFacade smartAppliancesFacade = new SmartAppliancesFacade();
        smartAppliancesFacade.say("打开");
        smartAppliancesFacade.say("关闭");
    }
}
