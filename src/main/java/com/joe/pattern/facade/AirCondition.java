package com.joe.pattern.facade;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class AirCondition {

    public void on(){
        System.out.println("打开空调");
    }

    public void off(){
        System.out.println("关闭空调");
    }
}
