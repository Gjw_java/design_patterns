package com.joe.pattern.facade;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class TV {

    public void on(){
        System.out.println("打开电视机");
    }

    public void off(){
        System.out.println("关闭电视机");
    }
}
