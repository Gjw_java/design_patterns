package com.joe.pattern.state.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public abstract class LiftState {
    protected Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public abstract void open();
    public abstract void close();
    public abstract void stop();
    public abstract void run();
}
