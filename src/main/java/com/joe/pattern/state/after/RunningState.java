package com.joe.pattern.state.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class RunningState extends LiftState{
    @Override
    public void open() {

    }

    @Override
    public void close() {

    }

    @Override
    public void stop() {
        super.context.setLiftState(Context.stoppingState);
        super.context.stop();
    }

    @Override
    public void run() {
        System.out.println("电梯正在运行");
    }
}
