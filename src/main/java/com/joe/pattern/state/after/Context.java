package com.joe.pattern.state.after;

import javafx.scene.paint.Stop;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Context {
    public static final OpeningState openingState = new OpeningState();
    public static final ClosingState closingState =  new ClosingState();
    public static final RunningState runningState = new RunningState();
    public static final StoppingState stoppingState = new StoppingState();

    private LiftState liftState;

    public LiftState getLiftState() {
        return liftState;
    }

    public void setLiftState(LiftState liftState) {
        this.liftState = liftState;
        this.liftState.setContext(this);
    }

    public void open(){
        this.liftState.open();
    }

    public void close(){
        this.liftState.close();
    }

    public void stop(){
        this.liftState.stop();
    }

    public void run(){
        this.liftState.run();
    }
}
