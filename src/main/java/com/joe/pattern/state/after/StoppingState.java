package com.joe.pattern.state.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class StoppingState extends LiftState{
    @Override
    public void open() {
        super.context.setLiftState(Context.openingState);
        super.context.open();
    }

    @Override
    public void close() {
        super.context.setLiftState(Context.closingState);
        super.context.close();
    }

    @Override
    public void stop() {
        System.out.println("电梯停止了");
    }

    @Override
    public void run() {
        super.context.setLiftState(Context.runningState);
        super.context.run();
    }
}
