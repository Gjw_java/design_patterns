package com.joe.pattern.state.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class OpeningState extends LiftState{
    @Override
    public void open() {
        System.out.println("电梯开启");
    }

    @Override
    public void close() {
        super.context.setLiftState(Context.closingState);
        super.context.close();
    }

    @Override
    public void stop() {

    }

    @Override
    public void run() {

    }
}
