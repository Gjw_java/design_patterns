package com.joe.pattern.state.after;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Client {

    public static void main(String[] args) {
        Context context = new Context();
        context.setLiftState(new RunningState());
        context.close();
        context.run();
        context.stop();
        context.open();
    }
}
