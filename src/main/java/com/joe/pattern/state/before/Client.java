package com.joe.pattern.state.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Client {

    public static void main(String[] args) {
        ILift lift = new Lift();
        lift.setState(ILift.OPENING_STATE);
        lift.open();
        lift.close();
        lift.run();
        lift.stop();
    }
}
