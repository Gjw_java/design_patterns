package com.joe.pattern.state.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public interface ILift {

    int OPENING_STATE = 1;
    int CLOSING_STATE = 2;
    int RUNNING_STATE = 3;
    int STOPPING_STATE = 4;

    void setState(int state);

    void open();

    void close();

    void stop();

    void run();

}
