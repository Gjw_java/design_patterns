package com.joe.pattern.state.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Lift implements ILift{

    private int state;

    @Override
    public void setState(int state) {
        this.state = state;
    }

    @Override
    public void open() {
        switch (state){
            case OPENING_STATE:
            case RUNNING_STATE:
                break;
            case CLOSING_STATE:
            case STOPPING_STATE:
                setState(OPENING_STATE);
                System.out.println("电梯打开了");
                break;
            default:
                System.out.println("未知状态");
        }
    }

    @Override
    public void close() {
        switch (state){
            case OPENING_STATE:
                setState(CLOSING_STATE);
                System.out.println("电梯关闭了");
                break;
            case RUNNING_STATE:
            case CLOSING_STATE:
            case STOPPING_STATE:
                break;
            default:
                System.out.println("未知状态");
        }
    }

    @Override
    public void stop() {
        switch (state){
            case OPENING_STATE:
            case STOPPING_STATE:
                break;
            case RUNNING_STATE:
            case CLOSING_STATE:
                setState(CLOSING_STATE);
                System.out.println("电梯停止了");
                break;
            default:
                System.out.println("未知状态");
        }
    }

    @Override
    public void run() {
        switch (state){
            case OPENING_STATE:
            case RUNNING_STATE:
                break;
            case CLOSING_STATE:
            case STOPPING_STATE:
                setState(RUNNING_STATE);
                System.out.println("电梯运行了");
                break;
            default:
                System.out.println("未知状态");
        }
    }
}
