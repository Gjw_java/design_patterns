package com.joe.pattern.decorator;

/**
 * @Description 炒面 具体构件角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class FiredNoodles extends FastFood{

    public FiredNoodles() {
        super(15, "炒面");
    }

    @Override
    public float cost() {
        return getPrice();
    }
}
