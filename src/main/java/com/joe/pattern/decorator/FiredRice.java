package com.joe.pattern.decorator;

/**
 * @Description 炒饭 具体构件角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class FiredRice extends FastFood {

    public FiredRice() {
        super(10, "炒饭");
    }

    @Override
    public float cost() {
        return getPrice();
    }
}
