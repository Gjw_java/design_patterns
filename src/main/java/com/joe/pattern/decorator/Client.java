package com.joe.pattern.decorator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Client {
    public static void main(String[] args) {
        FastFood food = new FiredRice();
        food = new Egg(food);
        food = new Bacon(food);
        System.out.println(food.getDesc() + " " + food.cost() + "元");
    }
}
