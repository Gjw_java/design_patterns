package com.joe.pattern.decorator;

/**
 * @Description 培根类 具体装饰者类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Bacon extends Garnish{

    public Bacon(FastFood fastFood) {
        super(2, "培根", fastFood);
    }

    @Override
    public float cost() {
        return getPrice() + getFastFood().cost();
    }

    @Override
    public String getDesc() {
        return super.getDesc();
    }
}
