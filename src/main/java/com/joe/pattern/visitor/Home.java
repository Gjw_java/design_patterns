package com.joe.pattern.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Home {

    private List<Animal> nodeList = new ArrayList<>(10);

    public void accept(Person person){
        for (Animal animal : nodeList) {
            animal.accept(person);
        }
    }

    public void add(Animal animal){
        nodeList.add(animal);
    }
}
