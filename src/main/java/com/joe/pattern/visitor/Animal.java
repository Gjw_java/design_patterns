package com.joe.pattern.visitor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public interface Animal {

    void accept(Person person);
}
