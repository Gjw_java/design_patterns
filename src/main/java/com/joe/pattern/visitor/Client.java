package com.joe.pattern.visitor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Client {
    public static void main(String[] args) {
        Home home = new Home();
        home.add(new Cat());
        home.add(new Dog());
        home.accept(new Owner());
        home.accept(new Someone());
    }
}
