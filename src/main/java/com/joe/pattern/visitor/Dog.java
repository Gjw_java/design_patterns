package com.joe.pattern.visitor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Dog implements Animal{

    @Override
    public void accept(Person person) {
        person.feed(this);
        System.out.println("好好吃，汪汪");
    }
}
