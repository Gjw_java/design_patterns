package com.joe.pattern.visitor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Someone implements Person{
    @Override
    public void feed(Cat cat) {
        System.out.println("其他人喂食猫");
    }

    @Override
    public void feed(Dog dog) {
        System.out.println("其他人喂食狗");
    }
}
