package com.joe.pattern.visitor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public interface Person {

    void feed(Cat cat);

    void feed(Dog dog);
}
