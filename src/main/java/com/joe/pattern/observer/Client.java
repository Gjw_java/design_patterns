package com.joe.pattern.observer;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public class Client {
    public static void main(String[] args) {
        SubscriptionSubject subscriptionSubject = new SubscriptionSubject();
        subscriptionSubject.attach(new WeixinUser("悟空"));
        subscriptionSubject.attach(new WeixinUser("八戒"));
        subscriptionSubject.attach(new WeixinUser("沙僧"));
        subscriptionSubject.notify("出发取经");
    }
}
