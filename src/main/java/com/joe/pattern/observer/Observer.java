package com.joe.pattern.observer;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-12
 */
public interface Observer {

    void update(String message);
}
