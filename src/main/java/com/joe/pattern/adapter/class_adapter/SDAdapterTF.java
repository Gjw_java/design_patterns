package com.joe.pattern.adapter.class_adapter;

/**
 * @Description 适配器类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class SDAdapterTF extends TFCardImpl implements SDCard{

    @Override
    public String readSD() {
        System.out.println("adapter tf read");
        return readTF();
    }

    @Override
    public void writeSD(String data) {
        System.out.println("adapter tf write");
        writeTF(data);
    }
}
