package com.joe.pattern.adapter.class_adapter;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Client {

    public static void main(String[] args) {
        Computer computer = new Computer();
//        String data = computer.readSD(new SDCardImpl());
        String data = computer.readSD(new SDAdapterTF());
        System.out.println(data);
    }
}
