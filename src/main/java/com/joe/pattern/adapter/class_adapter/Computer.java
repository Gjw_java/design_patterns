package com.joe.pattern.adapter.class_adapter;

/**
 * @Description 计算机类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Computer {

    public String readSD(SDCard sdCard){
        if (sdCard == null){
            throw new NullPointerException("SDCard is not null");
        }
        return sdCard.readSD();
    }
}
