package com.joe.pattern.adapter.class_adapter;

/**
 * @Description 适配者
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public interface TFCard {

    String readTF();

    void writeTF(String data);
}
