package com.joe.pattern.adapter.class_adapter;

/**
 * @Description 适配者类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class TFCardImpl implements TFCard{

    @Override
    public String readTF() {
        return "TFCard read data";
    }

    @Override
    public void writeTF(String data) {
        System.out.println("TFCard write:" + data);
    }
}
