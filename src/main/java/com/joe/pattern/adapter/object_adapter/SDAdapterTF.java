package com.joe.pattern.adapter.object_adapter;

/**
 * @Description 适配器类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class SDAdapterTF implements SDCard {

    private TFCard tfCard;

    public SDAdapterTF(TFCard tfCard){
        this.tfCard = tfCard;
    }

    @Override
    public String readSD() {
        System.out.println("adapter tf read");
        return tfCard.readTF();
    }

    @Override
    public void writeSD(String data) {
        System.out.println("adapter tf write");
        tfCard.writeTF(data);
    }
}
