package com.joe.pattern.adapter.object_adapter;

/**
 * @Description 具体的 SD卡类
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class SDCardImpl implements SDCard {
    @Override
    public String readSD() {
        return "SDCard read data";
    }

    @Override
    public void writeSD(String data) {
        System.out.println("SDCard write:" + data);
    }
}
