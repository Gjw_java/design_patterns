package com.joe.pattern.adapter.object_adapter;

/**
 * @Description 目标接口
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public interface SDCard {

    String readSD();

    void writeSD(String data);
}
