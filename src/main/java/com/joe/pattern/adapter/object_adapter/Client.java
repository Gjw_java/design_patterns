package com.joe.pattern.adapter.object_adapter;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Client {

    public static void main(String[] args) {
        Computer computer = new Computer();
//        String data = computer.readSD(new SDCardImpl());
        String data = computer.readSD(new SDAdapterTF(new TFCardImpl()));
        System.out.println(data);
    }
}
