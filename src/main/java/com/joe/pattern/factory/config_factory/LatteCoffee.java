package com.joe.pattern.factory.config_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class LatteCoffee extends Coffee {
    @Override
    public String getName() {
        return "拿铁";
    }
}
