package com.joe.pattern.factory.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class CoffeeStore {

    public Coffee orderCoffee(String type){
        Coffee coffee = null;
        if ("American".equals(type)){
            coffee = new AmericanCoffee();
        } else if ("Latte".equals(type)){
            coffee = new LatteCoffee();
        } else {
            throw new RuntimeException("您点的咖啡，没有了");
        }

        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
