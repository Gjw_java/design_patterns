package com.joe.pattern.factory.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Client {

    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        Coffee coffee = store.orderCoffee("American");
        System.out.println(coffee.getName());
    }
}
