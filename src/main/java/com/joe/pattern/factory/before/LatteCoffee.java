package com.joe.pattern.factory.before;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class LatteCoffee extends Coffee{
    @Override
    public String getName() {
        return "拿铁";
    }
}
