package com.joe.pattern.factory.factory_method;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Client {

    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        CoffeeFactory factory = new AmericanCoffeeFactory();
        store.setFactory(factory);
        Coffee coffee = store.orderCoffee();

        System.out.println(coffee.getName());
    }
}
