package com.joe.pattern.factory.factory_method;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class AmericanCoffeeFactory implements CoffeeFactory {
    @Override
    public Coffee createCoffee() {
        return new AmericanCoffee();
    }
}
