package com.joe.pattern.factory.abstract_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public abstract class Dessert {

    public abstract void show();
}
