package com.joe.pattern.factory.abstract_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Tiramisu extends Dessert{
    @Override
    public void show() {
        System.out.println("提拉米苏");
    }
}
