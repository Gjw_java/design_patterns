package com.joe.pattern.factory.abstract_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public abstract class Coffee {

    public abstract String getName();

    public void addSugar(){
        System.out.println("加入糖");
    }

    public void addMilk(){
        System.out.println("加入奶");
    }
}
