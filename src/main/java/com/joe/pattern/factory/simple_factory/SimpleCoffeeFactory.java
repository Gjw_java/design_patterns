package com.joe.pattern.factory.simple_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class SimpleCoffeeFactory {

    public Coffee createCoffee(String type){
        Coffee coffee = null;
        if ("American".equals(type)){
            coffee = new AmericanCoffee();
        } else if ("Latte".equals(type)){
            coffee = new LatteCoffee();
        } else {
            throw new RuntimeException("您点的咖啡，没有了");
        }

        return coffee;

    }
}
