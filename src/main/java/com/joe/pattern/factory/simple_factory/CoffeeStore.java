package com.joe.pattern.factory.simple_factory;


/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class CoffeeStore {

    public Coffee orderCoffee(String type){
        SimpleCoffeeFactory factory = new SimpleCoffeeFactory();
        Coffee coffee = factory.createCoffee(type);

        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
