package com.joe.pattern.factory.simple_factory;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Client {

    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        Coffee coffee = store.orderCoffee("Latte");
        System.out.println(coffee);
    }
}
