package com.joe.pattern.proxy.static_proxy;

import javax.annotation.Resource;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-08
 */
public class ProxyPoint implements SellTickets{

    private final TrainStation trainStation = new TrainStation();

    @Override
    public void sell() {
        System.out.println("代售点收取一些手续费用");
        trainStation.sell();
    }
}
