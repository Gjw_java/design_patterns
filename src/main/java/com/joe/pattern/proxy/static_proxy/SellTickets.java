package com.joe.pattern.proxy.static_proxy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-08
 */
public interface SellTickets {

    void sell();
}
