package com.joe.pattern.proxy.jdk_proxy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-08
 */
public class Client {
    public static void main(String[] args) {
        ProxyFactory proxyFactory = new ProxyFactory();
        SellTickets proxyObject = proxyFactory.getProxyObject();
        proxyObject.sell();
    }
}
