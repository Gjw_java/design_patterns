package com.joe.pattern.proxy.cglib_proxy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-08
 */
public class TrainStation {

    public void sell() {
        System.out.println("火车站卖票");
    }
}
