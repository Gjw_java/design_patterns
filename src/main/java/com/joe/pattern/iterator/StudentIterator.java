package com.joe.pattern.iterator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public interface StudentIterator {
    boolean hasNext();
    Student next();
}
