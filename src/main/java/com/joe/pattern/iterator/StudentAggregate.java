package com.joe.pattern.iterator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public interface StudentAggregate {
    void addStudent(Student student);
    void removeStudent(Student student);
    StudentIterator getStudentIterator();
}
