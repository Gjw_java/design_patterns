package com.joe.pattern.iterator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Client {

    public static void main(String[] args) {
        StudentAggregate studentAggregate = new StudentAggregateImpl();
        studentAggregate.addStudent(new Student("张三", "111"));
        studentAggregate.addStudent(new Student("李四", "2222"));
        StudentIterator studentIterator = studentAggregate.getStudentIterator();
        while (studentIterator.hasNext()){
            System.out.println(studentIterator.next());
        }
    }
}
