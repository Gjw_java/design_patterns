package com.joe.pattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class StudentAggregateImpl implements StudentAggregate{

    private List<Student> list = new ArrayList<>(10);

    @Override
    public void addStudent(Student student) {
        list.add(student);
    }

    @Override
    public void removeStudent(Student student) {
        list.remove(student);
    }

    @Override
    public StudentIterator getStudentIterator() {
        return new StudentIteratorImpl(list);
    }
}
