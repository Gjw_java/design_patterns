package com.joe.pattern.iterator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Student {
    private String name;
    private String Number;

    public Student(String name, String number) {
        this.name = name;
        Number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", Number='" + Number + '\'' +
                '}';
    }
}
