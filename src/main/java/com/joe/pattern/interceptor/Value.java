package com.joe.pattern.interceptor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Value extends AbstractExpression {

    private int value;

    public Value(int value) {
        this.value = value;
    }


    @Override
    public int interpret(Context context) {
        return value;
    }
}
