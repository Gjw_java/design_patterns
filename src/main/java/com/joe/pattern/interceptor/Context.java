package com.joe.pattern.interceptor;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Context {

    private Map<Variable, Integer> map = new HashMap<>(10);

    public void assign(Variable variable, Integer value){
        map.put(variable, value);
    }

    public int getValue(Variable variable){
        return map.get(variable);
    }
}
