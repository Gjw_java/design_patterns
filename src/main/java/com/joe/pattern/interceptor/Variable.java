package com.joe.pattern.interceptor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Variable extends AbstractExpression{

    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public int interpret(Context context) {
        return context.getValue(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
