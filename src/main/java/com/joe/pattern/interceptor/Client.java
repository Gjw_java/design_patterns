package com.joe.pattern.interceptor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Client {

    public static void main(String[] args) {
        Context context = new Context();

        Variable a = new Variable("a");
        Variable b = new Variable("b");
        Variable c = new Variable("c");
        Variable d = new Variable("d");
        Variable e = new Variable("e");

        context.assign(a, 1);
        context.assign(b,2);
        context.assign(c, 3);
        context.assign(d, 4);
        context.assign(e, 5);

        AbstractExpression abstractExpression = new Plus(a, new Minus(b,new Plus(c, new Plus(d,e))));
        int interpret = abstractExpression.interpret(context);
        System.out.println(interpret);
        System.out.println(1+2-(3+4+5) );
    }
}
