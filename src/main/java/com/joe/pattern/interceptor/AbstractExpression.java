package com.joe.pattern.interceptor;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public abstract class AbstractExpression {

    public abstract int interpret(Context context);
}
