package com.joe.pattern.builder.demo2;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Client {
    public static void main(String[] args) {
        Phone phone = new Phone.Builder()
                .cpu("intel")
                .screen("三星")
                .memory("金士顿")
                .mainboard("华硕")
                .build();

        System.out.println(phone);
    }
}
