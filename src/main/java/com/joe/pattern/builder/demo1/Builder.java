package com.joe.pattern.builder.demo1;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public abstract class Builder {

    protected Bike mbike = new Bike();

    public abstract void buildFrame();

    public abstract void buildSeat();

    public abstract Bike createBike();

    public Bike construct(){

        this.buildSeat();
        this.buildFrame();
        return this.createBike();
    }
}
