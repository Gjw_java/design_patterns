package com.joe.pattern.builder.demo1;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class MobikeBuilder extends Builder{

    @Override
    public void buildFrame() {
        mbike.setFrame("碳纤维车架");
    }

    @Override
    public void buildSeat() {
        mbike.setSeat("真皮车座");
    }

    @Override
    public Bike createBike() {
        return mbike;
    }
}
