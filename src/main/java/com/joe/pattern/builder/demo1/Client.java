package com.joe.pattern.builder.demo1;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Client {
    public static void main(String[] args) {
        Director director = new Director(new MobikeBuilder());
        Bike bike = director.construct();
        System.out.println(bike);
    }
}
