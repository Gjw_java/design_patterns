package com.joe.pattern.builder.demo1;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Director {

    private Builder builder;

    public Director(Builder builder){
        this.builder = builder;
    }

    public Bike construct(){
        builder.buildFrame();
        builder.buildSeat();
        return builder.createBike();
    }
}
