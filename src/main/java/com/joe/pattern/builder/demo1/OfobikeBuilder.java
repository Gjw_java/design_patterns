package com.joe.pattern.builder.demo1;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class OfobikeBuilder extends Builder{
    @Override
    public void buildFrame() {
        mbike.setFrame("铝合金车架");
    }

    @Override
    public void buildSeat() {
        mbike.setSeat("橡胶车座");
    }

    @Override
    public Bike createBike() {
        return mbike;
    }
}
