package com.joe.pattern.flyweight;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class Client {
    public static void main(String[] args) {
        BoxFactory instance = BoxFactory.getInstance();
        AbstractBox i = instance.getShape("I");
        i.display("red");
    }
}
