package com.joe.pattern.flyweight;

import java.util.HashMap;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class BoxFactory {

    private HashMap<String, AbstractBox> map;

    private static BoxFactory boxFactory = new BoxFactory();

    private BoxFactory(){
        map = new HashMap<>(1);
        map.put("I", new IBox());
        map.put("O", new OBox());
        map.put("L", new LBox());
    }

    public AbstractBox getShape(String name){
        return map.get(name);
    }

    public static BoxFactory getInstance(){
        return boxFactory;
    }
}
