package com.joe.pattern.flyweight;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public abstract class AbstractBox {

    public abstract String getShape();

    public void display(String color){
        System.out.println("方块形状："+ getShape() + ",颜色:" + color);
    }
}
