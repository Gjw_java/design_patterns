package com.joe.pattern.flyweight;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-10
 */
public class OBox extends AbstractBox{
    @Override
    public String getShape() {
        return "O";
    }
}
