package com.joe.pattern.prototype.demo;

/**
 * @Description 具体原型类
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Realizetype implements Cloneable {

    public Realizetype() {
        System.out.println("具体的原型对象创建成功了");
    }

    @Override
    public Realizetype clone() throws CloneNotSupportedException {
        System.out.println("具体原型复制成功！");
        return (Realizetype)super.clone();
    }
}
