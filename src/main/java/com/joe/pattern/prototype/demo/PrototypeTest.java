package com.joe.pattern.prototype.demo;

/**
 * @Description 测试访问类
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class PrototypeTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        Realizetype r1 = new Realizetype();
        Realizetype r2 = r1.clone();
        System.out.println(r1 == r2);
    }
}
