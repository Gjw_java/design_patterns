package com.joe.pattern.prototype.test1;

import java.io.Serializable;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Student implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
