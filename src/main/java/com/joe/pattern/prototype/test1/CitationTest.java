package com.joe.pattern.prototype.test1;

import java.io.*;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class CitationTest {

    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {
        Citation citation = new Citation();
        Student student = new Student();
        student.setName("stu1");
        citation.setStudent(student);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("/b.txt"));
        oos.writeObject(citation);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/b.txt"));
        Citation citation1 = (Citation) ois.readObject();
        ois.close();
        citation1.getStudent().setName("stu2");

        citation.show();
        citation1.show();
    }
}
