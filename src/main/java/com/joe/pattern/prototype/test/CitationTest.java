package com.joe.pattern.prototype.test;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class CitationTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        Citation citation = new Citation();
        Student student = new Student();
        student.setName("stu1");
//        citation.setName("stu1");
        citation.setStudent(student);
        Citation clone = citation.clone();
//        clone.setName("stu2");
        clone.getStudent().setName("stu2");

        citation.show();
        clone.show();
    }
}
