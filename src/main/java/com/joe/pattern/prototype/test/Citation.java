package com.joe.pattern.prototype.test;

/**
 * @Description 具体原型类
 * @Author 高建伟-joe
 * @Date 2024-01-05
 */
public class Citation implements Cloneable{

//    private String name;
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public void show(){
//        System.out.println(name + "同学是三好学生！");
//    }

    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void show(){
        System.out.println(student.getName() + "同学是三好学生！");
    }

    @Override
    public Citation clone() throws CloneNotSupportedException {
        return (Citation) super.clone();
    }
}
