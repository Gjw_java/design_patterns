package com.joe.pattern.memento.white_box;

import javax.management.relation.Role;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Client {
    public static void main(String[] args) {
        GameRole gameRole = new GameRole();
        gameRole.initState();
        gameRole.stateDisplay();

        RoleStateCaretaker roleStateCaretaker = new RoleStateCaretaker();
        RoleStateMemento roleStateMemento = gameRole.saveState();
        roleStateCaretaker.setRoleStateMemento(roleStateMemento);


        gameRole.fight();
        gameRole.stateDisplay();

        gameRole.recoverState(roleStateCaretaker.getRoleStateMemento());
        gameRole.stateDisplay();
    }
}
