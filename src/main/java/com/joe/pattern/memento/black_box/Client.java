package com.joe.pattern.memento.black_box;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class Client {
    public static void main(String[] args) {
        GameRole gameRole = new GameRole();
        gameRole.initState();
        gameRole.stateDisplay();

        RoleStateCaretaker roleStateCaretaker = new RoleStateCaretaker();
        Memento roleStateMemento = gameRole.saveState();
        roleStateCaretaker.setMemento(roleStateMemento);


        gameRole.fight();
        gameRole.stateDisplay();

        gameRole.recoverState(roleStateCaretaker.getMemento());
        gameRole.stateDisplay();
    }
}
