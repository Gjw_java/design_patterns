package com.joe.pattern.memento.black_box;

import com.joe.pattern.memento.white_box.RoleStateMemento;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class RoleStateCaretaker {
    private Memento memento;

    public Memento getMemento() {
        return memento;
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
}
