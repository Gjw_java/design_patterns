package com.joe.pattern.memento.black_box;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-16
 */
public class GameRole {
    private int vit;
    private int atk;
    private int def;

    public int getVit() {
        return vit;
    }

    public void setVit(int vit) {
        this.vit = vit;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    public int getDef() {
        return def;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void initState(){
        System.out.println("游戏角色初始化");
        this.atk = 100;
        this.vit = 100;
        this.def = 100;
    }

    public void fight(){
        System.out.println("游戏角色开始打斗");
        this.atk = 0;
        this.vit = 0;
        this.def = 0;
    }

    public Memento saveState(){
        System.out.println("保存状态");
        return new RoleStateMemento(vit, atk, def);
    }

    public void recoverState(Memento memento){
        System.out.println("恢复状态");
        RoleStateMemento roleStateMemento = (RoleStateMemento) memento;
        this.def = roleStateMemento.getDef();
        this.atk = roleStateMemento.getAtk();
        this.vit = roleStateMemento.getVit();
    }

    public void stateDisplay(){
        System.out.println("防御力：" + def);
        System.out.println("生命力：" + vit);
        System.out.println("攻击力：" + atk);
    }

    private class RoleStateMemento implements Memento{
        private int vit;
        private int atk;
        private int def;

        public RoleStateMemento() {
        }

        public RoleStateMemento(int vit, int atk, int def) {
            this.vit = vit;
            this.atk = atk;
            this.def = def;
        }

        public int getVit() {
            return vit;
        }

        public void setVit(int vit) {
            this.vit = vit;
        }

        public int getAtk() {
            return atk;
        }

        public void setAtk(int atk) {
            this.atk = atk;
        }

        public int getDef() {
            return def;
        }

        public void setDef(int def) {
            this.def = def;
        }
    }
}
