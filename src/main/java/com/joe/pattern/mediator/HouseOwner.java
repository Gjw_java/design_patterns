package com.joe.pattern.mediator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class HouseOwner extends Person {

    public HouseOwner(String name, Mediator mediator) {
        super(name, mediator);
    }

    public void constact(String message){
        mediator.constact(message, this);
    }

    public void getMessage(String message){
        System.out.println("房主"+ name + "获取到的信息是：" +message);
    }
}
