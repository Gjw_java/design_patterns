package com.joe.pattern.mediator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public abstract class Person {
    protected String name;
    protected Mediator mediator;

    public Person(String name, Mediator mediator) {
        this.name = name;
        this.mediator = mediator;
    }
}
