package com.joe.pattern.mediator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public abstract class Mediator {

    public abstract void constact(String message, Person person);
}
