package com.joe.pattern.mediator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class Client {

    public static void main(String[] args) {
        MediatorStructure mediatorStructure = new MediatorStructure();
        Tenant tenant = new Tenant("张三", mediatorStructure);
        HouseOwner houseOwner = new HouseOwner("李四", mediatorStructure);
        mediatorStructure.setTenant(tenant);
        mediatorStructure.setHouseOwner(houseOwner);
        tenant.constact("我要租两室一厅");
        houseOwner.constact("我这里有两套房，你要租吗？");
    }
}
