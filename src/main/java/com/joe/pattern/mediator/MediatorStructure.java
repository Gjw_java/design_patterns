package com.joe.pattern.mediator;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-15
 */
public class MediatorStructure extends Mediator{

    private Tenant tenant;
    private HouseOwner houseOwner;

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }

    public HouseOwner getHouseOwner() {
        return houseOwner;
    }

    public void setHouseOwner(HouseOwner houseOwner) {
        this.houseOwner = houseOwner;
    }

    @Override
    public void constact(String message, Person person) {
        if (person == houseOwner){
            houseOwner.getMessage(message);
        } else {
            tenant.getMessage(message);
        }
    }
}
