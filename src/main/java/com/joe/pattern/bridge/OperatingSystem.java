package com.joe.pattern.bridge;

/**
 * @Description 抽象操作系统类 抽象化角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public abstract class OperatingSystem {

    protected VideoFile videoFile;

    public OperatingSystem(VideoFile videoFile) {
        this.videoFile = videoFile;
    }

    public abstract void play(String fileName);
}
