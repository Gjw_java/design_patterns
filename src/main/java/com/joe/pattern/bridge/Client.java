package com.joe.pattern.bridge;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Client {
    public static void main(String[] args) {
        VideoFile file = new AVIFile();
        OperatingSystem system = new Mac(file);
        system.play("战狼3");
    }
}
