package com.joe.pattern.bridge;

/**
 * @Description AVI 视频文件 具体的实现化角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class AVIFile implements VideoFile{
    @Override
    public void decode(String fileName) {
        System.out.println("avi 视频文件"+ fileName);
    }
}
