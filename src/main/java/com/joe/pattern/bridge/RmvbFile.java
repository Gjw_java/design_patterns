package com.joe.pattern.bridge;

/**
 * @Description Rmvb 视频文件 具体实现化角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class RmvbFile implements VideoFile{
    @Override
    public void decode(String fileName) {
        System.out.println("Rmvb 视频文件" + fileName);
    }
}
