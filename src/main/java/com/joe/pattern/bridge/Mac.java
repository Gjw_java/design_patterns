package com.joe.pattern.bridge;

/**
 * @Description Mac系统 扩展抽象化角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public class Mac extends OperatingSystem{

    public Mac(VideoFile videoFile) {
        super(videoFile);
    }

    @Override
    public void play(String fileName) {
        videoFile.decode(fileName);
    }
}
