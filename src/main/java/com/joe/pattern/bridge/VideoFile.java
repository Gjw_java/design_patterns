package com.joe.pattern.bridge;

/**
 * @Description 视频文件 实现化角色
 * @Author 高建伟-joe
 * @Date 2024-01-09
 */
public interface VideoFile {

    void decode(String fileName);
}
