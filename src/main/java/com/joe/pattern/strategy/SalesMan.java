package com.joe.pattern.strategy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class SalesMan {

    private Strategy strategy;

    public SalesMan(Strategy strategy){
        this.strategy = strategy;
    }

    public void salesManShow(){
        strategy.show();
    }
}
