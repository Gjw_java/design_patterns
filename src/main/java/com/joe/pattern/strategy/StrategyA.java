package com.joe.pattern.strategy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class StrategyA implements Strategy{
    @Override
    public void show() {
        System.out.println("买一送一");
    }
}
