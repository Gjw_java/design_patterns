package com.joe.pattern.strategy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class StrategyC implements Strategy {
    @Override
    public void show() {
        System.out.println("满1000加1元换购任意200元以下商品");
    }
}
