package com.joe.pattern.strategy;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public interface Strategy {

    void show();
}
