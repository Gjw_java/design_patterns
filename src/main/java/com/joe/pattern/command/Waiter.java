package com.joe.pattern.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class Waiter {

    private List<Command> commands = new ArrayList<>(1);

    public void setCommands(Command command) {
        this.commands.add(command);
    }

    public void orderUp(){
        for (Command command : commands) {
            command.execute();
        }
    }
}
