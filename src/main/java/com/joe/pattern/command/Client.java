package com.joe.pattern.command;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class Client {

    public static void main(String[] args) {
        Waiter waiter = new Waiter();
        Order order = new Order();
        order.setDiningTable(1);
        order.setFoodDic("汉堡", 1);
        order.setFoodDic("薯条", 19);
        OrderCommand orderCommand = new OrderCommand(new SeniorChef(), order);

        waiter.setCommands(orderCommand);
        waiter.orderUp();

    }
}
