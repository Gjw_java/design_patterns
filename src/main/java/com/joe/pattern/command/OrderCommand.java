package com.joe.pattern.command;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class OrderCommand implements Command{

    private SeniorChef receiver;
    private Order order;

    public OrderCommand(SeniorChef receiver, Order order){
        this.receiver = receiver;
        this.order = order;
    }

    @Override
    public void execute() {
        for (String key : order.getFoodDic().keySet()) {
            receiver.makeFood(key, order.getFoodDic().get(key));
        }
    }
}
