package com.joe.pattern.command;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class Order {
    private int diningTable;
    private Map<String, Integer> foodDic = new HashMap<>(1);

    public int getDiningTable() {
        return diningTable;
    }

    public void setDiningTable(int diningTable) {
        this.diningTable = diningTable;
    }

    public Map<String, Integer> getFoodDic() {
        return foodDic;
    }

    public void setFoodDic(String name, int num) {
        this.foodDic.put(name, num);
    }
}
