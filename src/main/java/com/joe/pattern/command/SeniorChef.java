package com.joe.pattern.command;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2024-01-11
 */
public class SeniorChef {

    public void makeFood(String name, int num){
        System.out.println(num + "份" + name);
    }
}
