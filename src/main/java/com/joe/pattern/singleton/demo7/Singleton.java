package com.joe.pattern.singleton.demo7;

/**
 * @Description 饿汉式 枚举方式
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public enum Singleton {
    INSTANCE;
}
