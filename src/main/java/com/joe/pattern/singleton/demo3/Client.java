package com.joe.pattern.singleton.demo3;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Client {
    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();

        Singleton instance1 = Singleton.getInstance();
        System.out.println(instance==instance1);
    }
}
