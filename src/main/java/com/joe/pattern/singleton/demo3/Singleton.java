package com.joe.pattern.singleton.demo3;

/**
 * @Description 懒汉式 线程不安全
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Singleton {

    // 1.私有构造方法
    private Singleton(){}

    // 2.声明Singleton类型的变量
    private static Singleton instance;

    // 3.对外提供访问方式
    public static synchronized Singleton getInstance(){
        if (instance == null){
            // 线程1等待，线程2获取到cpu的执行权，也会进入到该判断里面
            instance = new Singleton();
        }
        return instance;
    }
}
