package com.joe.pattern.singleton.demo5;

/**
 * @Description 懒汉式 双重检查锁
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Singleton {

    // 1.私有构造方法
    private Singleton(){}

    // 2.声明Singleton类型的变量
    private static volatile Singleton instance;

    // 3.对外提供访问方式
    public static Singleton getInstance(){
        // 第一次判断，如果 instance 的值不为 null, 不需要抢占锁，直接返回对象
        if (instance == null){
            synchronized (Singleton.class){
                if (instance == null){
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
