package com.joe.pattern.singleton.demo2;

import com.joe.pattern.singleton.demo2.Singleton;

/**
 * @Description
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Client {
    public static void main(String[] args) {
        // 创建 Singleton 对象
        Singleton instance = Singleton.getInstance();

        Singleton instance1 = Singleton.getInstance();

        System.out.println(instance==instance1);
    }
}
