package com.joe.pattern.singleton.demo2;

/**
 * @Description 单例模式：饿汉式 静态代码块
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Singleton {

    // 1.私有构造方法
    private Singleton(){}

    // 2.声明Singleton类型的变量
    private static Singleton instance;

    // 3.在静态代码块中赋值
    static {
        instance = new Singleton();
    }

    // 4.对外提供获取该类对象的方法
    public static Singleton getInstance(){
        return instance;
    }
}
