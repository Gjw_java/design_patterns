package com.joe.pattern.singleton.demo6;

/**
 * @Description 饿汉式 静态内部类的方式
 * 静态内部类单例模式中实例由内部类创建，
 * 由于 JVM 在加载外部类的过程中，是不会加载静态内部类的，只有内部类的属性/方法被调用时才会被加载，并初始化其静态属性。
 * 静态属性由于被 static 修饰，保证只被实例化一次，并且严格保证实例化顺序。
 * @Author 高建伟-joe
 * @Date 2023-12-27
 */
public class Singleton {

    // 1. 私有构造方法
    private Singleton(){}

    //2. 静态内部类中创建对象
    private static class SingletonHolder{
        private static final Singleton INSTANCE = new Singleton();
    }

    // 3. 对外提供静态方法获取该对象
    public static Singleton getInstance(){
        return SingletonHolder.INSTANCE;
    }

}
