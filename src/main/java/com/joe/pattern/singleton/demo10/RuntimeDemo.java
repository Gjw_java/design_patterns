package com.joe.pattern.singleton.demo10;

import java.io.InputStream;

/**
 * @Description Runtime 类的使用
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class RuntimeDemo {

    public static void main(String[] args) throws Exception {
        Runtime runtime = Runtime.getRuntime();

        // 返回 java 虚拟机中的内存总量
        System.out.println(runtime.totalMemory());
        // 返回 java 虚拟机试图使用的最大内存量
        System.out.println(runtime.maxMemory());

        // 调用 runtime 的 exec 方法，参数是一个命令
        Process ipconfig = runtime.exec("ipconfig");
        // Process 对象的获取输入流的方法
        InputStream inputStream = ipconfig.getInputStream();
        byte[] arr = new byte[1024 * 1024 * 100];
        // 读取数据
        int len = inputStream.read(arr); // 返回读到的字节个数
        // 将字节数组转化为字符串输出到控制台
        System.out.println(new String(arr,0, len, "GBK"));
    }
}
