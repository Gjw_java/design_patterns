package com.joe.pattern.singleton.demo8;

import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @Description 懒汉式 静态内部类
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Singleton implements Serializable {

    private Singleton(){}

    private static class SingletonHolder{
        private static final Singleton INSTANCE = new Singleton();
    }

    public static Singleton getInstance(){
        return SingletonHolder.INSTANCE;
    }

    // 当进行反序列化时，会自动调用该方法，将该方法的返回值直接返回。
    public Object readResolve(){
        return SingletonHolder.INSTANCE;
    }
}
