package com.joe.pattern.singleton.demo8;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @Description 序列化和反序列化 破坏单例类 测试类
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Client {

    public static void main(String[] args) throws Exception {
//        writeObj2File();
        readObjFromFile();
        readObjFromFile();
    }

    // 向文件中写对象
    public static void writeObj2File() throws Exception{
        Singleton singleton = Singleton.getInstance();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./a.txt"));
        oos.writeObject(singleton);
        oos.close();
    }
    // 从文件中读对象
    public static void readObjFromFile() throws Exception{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./a.txt"));
        Singleton singleton = (Singleton) ois.readObject();
        System.out.println(singleton);
        ois.close();
    }
}
