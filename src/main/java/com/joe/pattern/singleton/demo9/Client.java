package com.joe.pattern.singleton.demo9;

import java.lang.reflect.Constructor;

/**
 * @Description 反射 破坏单例模式 测试类
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Client {

    public static void main(String[] args) throws Exception {
        // 1.获取Singleton 的字节码对象
        Class<Singleton> clazz = Singleton.class;
        // 2.获取无参构造方法
        Constructor<Singleton> declaredConstructor = clazz.getDeclaredConstructor();
        // 3.取消访问检查
        declaredConstructor.setAccessible(true);
        // 4.创建 Singleton 对象
        Singleton singleton = declaredConstructor.newInstance();
        Singleton singleton1 = declaredConstructor.newInstance();

        System.out.println(singleton == singleton1);
    }
}
