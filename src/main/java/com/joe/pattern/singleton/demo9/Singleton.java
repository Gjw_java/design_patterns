package com.joe.pattern.singleton.demo9;

/**
 * @Description 懒汉式 静态内部类
 * @Author 高建伟-joe
 * @Date 2023-12-28
 */
public class Singleton {
    private static boolean flag = false;

    private Singleton(){
        synchronized (Singleton.class){
            // 判断 flag 的值是否为 true,如果是 true,说明非第一次访问，直接抛一个异常，如果是 false ,说明是第一次访问。
            if (flag){
                throw new RuntimeException("不能创建多个对象");
            }
            // 将 flag 的值设为 true
            flag = true;
        }
    }

    private static class SingletonHolder{
        private static final Singleton INSTANCE = new Singleton();
    }

    public static Singleton getInstance(){
        return SingletonHolder.INSTANCE;
    }
}
